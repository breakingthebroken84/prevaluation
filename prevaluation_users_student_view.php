<?php

/*
 * Plugin PREVALUATION dependency
 * Extension of moodleform
 * students view
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once 'locallib.php';
require_once $CFG->libdir.'/formslib.php';
require_once $CFG->libdir.'/gradelib.php';

global $PAGE;
$PAGE->requires->css("/mod/prevaluation/assets/prevaluation.css");

class prevaluation_users_student_view extends moodleform
{
    function definition (){
        global $COURSE, $DB, $PAGE, $CFG, $USER;
        
        $mformusers =& $this->_form;
        $mformusers->addElement('html', '<h3>'.get_string('enrolling_users','prevaluation').'</h3>');

        //fetching enrolled users
        $item_instance_id = $DB->get_record(
            'course_modules',
            array(
                'id' => $_GET['id']
            )
        )->instance;

        //echo $item_instance_id;

        $grade_item_id = $DB->get_record(
            'grade_items',
            array(
                'iteminstance' => $item_instance_id,
                'itemmodule' => 'prevaluation'
            )
        )->id;


        $grades = $DB->get_records(
            'grade_grades',
            array(
                'itemid' => $grade_item_id
            )
        );

        //fetching waiting users
        $waitingUsers = $DB->get_records(
            'prevaluation_waiting_users',
            array(
                //'course_id'=> $features['id'],
                'instance_id' => $DB->get_record(
                    'course_modules',
                    array('id'=>$_GET['id'])
                )->instance
            )
        );
        
        /*
        $mformusers->addElement('html',
            '<p>
                <span>INT : studenti iscritti alla piattaforma</span><br>
                <span class="not-in-moodle">EXT : studenti non iscritti alla piattaforma<br>
            </p>');
        */

        $mformusers->addElement('html', '<table class="table table-condensed table-prevaluation-user">');
        $mformusers->addElement('html', '<thead>');
        $mformusers->addElement('html', '<tr>');
        $mformusers->addElement('html', '<td>nome</td>');
        $mformusers->addElement('html', '<td>cognome</td>');
        $mformusers->addElement('html', '<td>status</td>');
        $mformusers->addElement('html', '</tr>');
        $mformusers->addElement('html', '</thead>');

        $mformusers->addElement('html', '<tbody>');
        foreach ($grades as $key => $grade) {
            $moodle_user = $DB->get_record(
                'user', 
                array(
                    'id'=> $grade->userid
                )
            );

            $class = "in-moodle";
            $mformusers->addElement('html', '<tr class="user '.$class.'">');
            $mformusers->addElement('html', '<td>'.$moodle_user->firstname.'</td>');
            $mformusers->addElement('html', '<td>'.$moodle_user->lastname.'</td>');
            //$mformusers->addElement('html', '<td>'.intval($grade->finalgrade).'</td>');
            //$mformusers->addElement('html', '<td></td>');
            $mformusers->addElement('html', '<td><span class="user-badge">'.get_string('graded','prevaluation').'</span></td>');
            $mformusers->addElement('html', '</tr>');
        }

        foreach ($waitingUsers as $key => $user) {
            $moodle_user = $DB->get_record(
                'user', 
                array(
                    //'firstname'=> $user->user_name,
                    //'lastname'=> $user->user_surname,
                    'email'=> $user->user_email
                )
            );

            $class = ( $moodle_user ? "pending-in-moodle" : "pending-not-in-moodle");

            if(($moodle_user !== false) & ($moodle_user->firstname !== $user->user_name | $moodle_user->lastname !== $user->user_surname))
            {
                $class .= " user-with-errors";
            }

            $mformusers->addElement('html', '<tr class="user '.$class.'">');
            $mformusers->addElement('html', '<td>'.$user->user_name.'</td>');
            $mformusers->addElement('html', '<td>'.$user->user_surname.'</td>');
            //$mformusers->addElement('html', '<td>'.intval($user->user_grade).'</td>');
            //$mformusers->addElement('html', '<td></td>');
            $mformusers->addElement('html', '<td><span class="user-badge">'.($moodle_user ? get_string('pending_external','prevaluation') : get_string('pending_internal','prevaluation')).'</span></td>');
            $mformusers->addElement('html', '</tr>');
        }
        $mformusers->addElement('html', '</tbody>');
        $mformusers->addElement('html', '</table>');
    }
}