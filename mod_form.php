<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main prevaluation configuration form
 *
 * It uses the standard core Moodle formslib. For more info about them, please
 * visit: http://docs.moodle.org/en/Development:lib/formslib.php
 *
 * @package    mod_prevaluation
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form
 *
 * @package    mod_prevaluation
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_prevaluation_mod_form extends moodleform_mod {

    /**
     * Defines forms elements
     */
    public function definition() {
        global $DB, $CFG, $PAGE;

        $PAGE->requires->js("/mod/prevaluation/assets/prevaluation.js");
        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('prevaluationname', 'prevaluation'), array('size' => '64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'prevaluationname', 'prevaluation');

        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }

        // Adding the rest of prevaluation settings, spreading all them into this fieldset
        // ... or adding more fieldsets ('header' elements) if needed for better logic.
        //$mform->addElement('static', 'label1', 'prevaluationsetting1', 'Your prevaluation fields go here. Replace me!');
        $pluginconfig = get_config("prevaluation");

/*
        echo '<pre>';
        var_dump($pluginconfig);
        die();
*/
        //echo basename($_SERVER['PHP_SELF']);
        //die();

        $mform->addElement('header', 'settings', get_string('settings', 'prevaluation'));
        $options = array(
            '1' => get_string('show_more', 'prevaluation'),
            '0' => get_string('show_less', 'prevaluation')
        );

        $mform->addElement('select', 'showdetails', get_string('show_info', 'prevaluation'), $options);
        $mform->addHelpButton('showdetails', 'show_info', 'prevaluation');
        $mform->addElement('textarea', 'msg_1', get_string('msg_1_label', 'prevaluation'), 'wrap="virtual" rows="4" cols="50"');
        $mform->addElement('textarea', 'msg_2', get_string('msg_2_label', 'prevaluation'), 'wrap="virtual" rows="4" cols="50"');
        $mform->addElement('textarea', 'msg_3', get_string('msg_3_label', 'prevaluation'), 'wrap="virtual" rows="4" cols="50"');

        $mform->addElement('header', 'general', get_string('csv_mapping', 'prevaluation'));
        $options = array();
        for ($i=1;$i<=100;$i++)
        {
          array_push($options, $i);
        }

        $mform->addElement('select', 'csv_separator', get_string('separator','prevaluation'), 
            array(
                1 => get_string('two_dots','prevaluation'),
                2 => get_string('dot_and_comma','prevaluation'),
                3 => get_string('tab','prevaluation'),
                4 => get_string('comma','prevaluation')
            )
        );
        $mform->setDefault('csv_separator', $pluginconfig->separator);


        $mform->addElement('select', 'index_firstname', get_string('firstname'), $options);
        //$mform->setDefault('index_firstname', $pluginconfig->firstname);
        $mform->setDefault('index_firstname', 0);

        $mform->addElement('select', 'index_lastname', get_string('lastname'), $options);
        //$mform->setDefault('index_lastname', $pluginconfig->lastname);
        $mform->setDefault('index_lastname', 1);

        $mform->addElement('select', 'index_email', get_string('email'), $options);
        //$mform->setDefault('index_email', $pluginconfig->email);
        $mform->setDefault('index_email', 2);

        $mform->addElement('select', 'index_grade', get_string('grade'), $options);
        //$mform->setDefault('index_grade', $pluginconfig->grade);
        $mform->setDefault('index_grade', 3);


        // Add standard grading elements.
        $this->standard_grading_coursemodule_elements();

        // Add standard elements, common to all modules.
        $this->standard_coursemodule_elements();

        // Add standard buttons, common to all modules.
        $this->add_action_buttons();
    }
}
