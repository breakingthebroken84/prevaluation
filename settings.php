<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Url module admin settings and defaults
 *
 * @package    mod_url
 * @copyright  2009 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
    require_once("$CFG->libdir/resourcelib.php");

    //$settings->add(new admin_setting_configselect('book/numbering', get_string('numbering', 'mod_book'), '', BOOK_NUM_NUMBERS, $options));
    //$settings->add(new admin_setting_heading('bookmodeditdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));

    $settings->add(new admin_setting_heading('prevaluationdefaults', get_string('modeditdefaults', 'admin'), get_string('condifmodeditdefaults', 'admin')));
    $options = array(
      1 => get_string('two_dots','prevaluation'),
      2 => get_string('dot_and_comma','prevaluation'),
      3 => get_string('tab','prevaluation'),
      4 => get_string('comma','prevaluation')
    );
    $settings->add(new admin_setting_configselect('prevaluation/separator', get_string('separator', 'prevaluation'), get_string('separator_desc', 'prevaluation'), '', $options));


    $settings->add(new admin_setting_configcheckbox('prevaluation/activity_lock',
                                                    get_string('activity_lock', 'prevaluation'),
                                                    get_string('activity_lock_desc', 'prevaluation'),
                                                    0));


}
