<?php

/*
 * Plugin PREVALUATION dependency
 * Extension of moodleform
 * import form view
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once 'locallib.php';
require_once $CFG->libdir.'/formslib.php';
require_once $CFG->libdir.'/gradelib.php';

global $PAGE;
$PAGE->requires->css("/mod/prevaluation/assets/prevaluation.css");

class prevaluation_import_form extends moodleform {
    function definition (){
        global $COURSE, $DB;

        $mform =& $this->_form;
        $moduleid = $DB->get_record (
                    'course_modules',
                    array(
                        'id' => $_GET['id']
                    )
                )->instance;

        $instance_config = $DB->get_record(
            'prevaluation',
            array(
                'id' => $moduleid
            )
        );

        if (isset($this->_customdata)) {  // hardcoding plugin names here is hacky
            $features = $this->_customdata;
        } else {
            $features = array();
        }

        // course id needs to be passed for auth purposes
        $mform->addElement('hidden', 'id', optional_param('id', 0, PARAM_INT));
        $mform->setType('id', PARAM_INT);
        $mform->addElement('header', 'general', get_string('importfile', 'grades'));

        // Restrict the possible upload file types.
        if (!empty($features['acceptedtypes'])) {
            $acceptedtypes = $features['acceptedtypes'];
        } else {
            $acceptedtypes = '*';
        }

        // File upload.
        $mform->addElement('filepicker', 'userfile', get_string('file'), null, array('accepted_types' => $acceptedtypes));
        $mform->addRule('userfile', null, 'required');
        $encodings = core_text::get_encodings();
        $mform->addElement('select', 'encoding', get_string('encoding', 'grades'), $encodings);
        $mform->addHelpButton('encoding', 'encoding', 'grades');

        if (!empty($features['includeseparator'])) {
            $radio = array();
            $radio[] = $mform->createElement('radio', 'separator', null, get_string('septab', 'grades'), 'tab');
            $radio[] = $mform->createElement('radio', 'separator', null, get_string('sepcomma', 'grades'), 'comma');
            $radio[] = $mform->createElement('radio', 'separator', null, get_string('sepcolon', 'grades'), 'colon');
            $radio[] = $mform->createElement('radio', 'separator', null, get_string('sepsemicolon', 'grades'), 'semicolon');
            $mform->addGroup($radio, 'separator', get_string('separator', 'grades'), ' ', false);
            $mform->addHelpButton('separator', 'separator', 'grades');


            switch ($instance_config->csv_separator) {
                case 1:
                    $mform->setDefault('separator', 'colon');
                    break;
                case 2:
                    $mform->setDefault('separator', 'semicolon');
                    break;
                case 3:
                    $mform->setDefault('separator', 'tab');
                    break;
                case 4:
                    $mform->setDefault('separator', 'comma');
                    break;                    
                default:
                    $mform->setDefault('separator', 'comma');
                    break;
            }



            //$mform->setDefault('separator', 'comma');
        }

        if (!empty($features['verbosescales'])) {
            $options = array(1=>get_string('yes'), 0=>get_string('no'));
            $mform->addElement('select', 'verbosescales', get_string('verbosescales', 'grades'), $options);
            $mform->addHelpButton('verbosescales', 'verbosescales', 'grades');
        }

        $options = array('10'=>10, '20'=>20, '100'=>100, '1000'=>1000, '100000'=>100000);
        $mform->addElement('select', 'previewrows', get_string('rowpreviewnum', 'grades'), $options); // TODO: localize
        $mform->addHelpButton('previewrows', 'rowpreviewnum', 'grades');
        $mform->setType('previewrows', PARAM_INT);
        //$mform->addElement('checkbox', 'forceimport', get_string('forceimport', 'grades'));
        $mform->addHelpButton('forceimport', 'forceimport', 'grades');
        $mform->setDefault('forceimport', false);
        $mform->setType('forceimport', PARAM_BOOL);
        $mform->addElement('hidden', 'groupid', groups_get_course_group($COURSE));
        $mform->addElement('hidden', 'instance_id', $_GET['id']);
        $mform->setType('groupid', PARAM_INT);

        $mform->addElement('hidden', 'instanceid', $features['instanceid']);
        
        $this->add_action_buttons(false, get_string('uploadgrades', 'grades'));
    }
}
