<?php

/*
 * Plugin PREVALUATION dependency
 * Extension of moodleform
 * import form (mapping field step) view
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once 'locallib.php';
require_once $CFG->libdir.'/formslib.php';
require_once $CFG->libdir.'/gradelib.php';

global $PAGE;
$PAGE->requires->css("/mod/prevaluation/assets/prevaluation.css");

class prevaluation_import_mapping_form extends moodleform {

    function definition () {
        global $CFG, $COURSE, $DB;

        $moduleid = $DB->get_record (
                    'course_modules',
                    array(
                        'id' => $_POST['id']
                    )
                )->instance;

        $instance_config = $DB->get_record(
            'prevaluation',
            array(
                'id' => $moduleid
            )
        );

        $mform =& $this->_form;


        // this is an array of headers
        $header = $this->_customdata['header'];
        // course id

        $mform->addElement('header', 'general', get_string('identifier', 'grades'));
        $mform->addElement('hidden', 'instanceid', $_POST['instanceid']);
        $mapfromoptions = array();
        $defaultFrom = 0;

        if ($header) {
            foreach ($header as $i=>$h) {
                $mapfromoptions[$i] = s($h);
                if(s($h) == get_string('email'))
                {
                    $defaultFrom = $i;
                }
            }
        }

        /*
        echo '<pre>';
        echo 'nome ';
        var_dump($instance_config->index_firstname);
        echo 'cognome ';
        var_dump($instance_config->index_lastname);
        echo 'email ';
        var_dump($instance_config->index_email);
        echo 'grade ';
        var_dump($instance_config->index_grade);
        echo '</pre>';
        var_dump($mapfromoptions);
        */

        $index_email = intval($instance_config->index_email);
        $index_firstname = intval($instance_config->index_firstname);
        $index_lastname = intval($instance_config->index_lastname);
        $index_grade = intval($instance_config->index_grade);
        
        $mform->addElement('select', 'mapfirstname', get_string('firstname'), $mapfromoptions);
        $mform->setDefault('mapfirstname', $index_firstname);

        $mform->addElement('select', 'maplastname', get_string('lastname'), $mapfromoptions);
        $mform->setDefault('maplastname', $index_lastname);

        $mform->addElement('select', 'mapfrom', (get_string('email') . ' *<p class="legend">'.get_string('legend_id', 'prevaluation').'</p>'), $mapfromoptions);
        $mform->setDefault('mapfrom', $index_email);

        $mform->addElement('select', 'mapgrade', (get_string('grade') . ' *<p class="legend">'.get_string('legend_grade', 'prevaluation').'</p>'), $mapfromoptions);
        $mform->setDefault('mapgrade', $index_grade);
        

        // EMAIL AS DEFAULT IDENTIFIER:
        //$mform->addHelpButton('mapfrom', 'mapfrom', 'grades');
        $mform->addElement('hidden', 'mapto', 'useremail');



        $maptooptions = array(
            //'userid'       => get_string('userid', 'grades'),
            //'username'     => get_string('username'),
            //'useridnumber' => get_string('idnumber'),
            'useremail'    => get_string('email'),
            //'0'            => get_string('ignore', 'grades')
        );

        $mform->addElement('select', 'mapto', get_string('mapto', 'prevaluation'), $maptooptions);
        //$mform->addHelpButton('mapto', 'mapto', 'grades');
        //$mform->addHelpButton('general_map', 'mappings', 'grades');
        //$mform->setDefault('mapfrom', $defaultFrom);

        // Add a feedback option.
        $feedbacks = array();
        if ($gradeitems = $this->_customdata['gradeitems']) {
            foreach ($gradeitems as $itemid => $itemname) {
                $feedbacks['feedback_'.$itemid] = get_string('feedbackforgradeitems', 'grades', $itemname);
            }
        }

        if ($header) {
            $i = 0; // index
            foreach ($header as $h) {
                $h = trim($h);
                // This is what each header maps to.
                $headermapsto = array(
                    //get_string('others', 'grades')     => array(
                    //    '0'   => get_string('ignore', 'grades'),
                    //    'new' => get_string('newitem', 'grades')
                    //),
                    get_string('gradeitems', 'grades') => $gradeitems,
                    //get_string('feedbacks', 'grades')  => $feedbacks
                );
                if($h == get_string('grade'))
                {

                    $activity_lock = (get_config('prevaluation')->activity_lock) ? 1 : null;
                    
                    if($activity_lock)
                    {
                        $mform->addElement('header', 'general_map', get_string('mappings', 'grades'));
                        $mform->addElement('selectgroups', 'mapping_'.$i, s($h), $headermapsto);
                        //$this->_form->disabledIf('mapping_'.$i, $activity_lock);
                    }

                    $instancename = $DB->get_record(
                        'prevaluation',
                        array(
                            'id' => $_POST['instanceid']
                        )
                    )->name;
                    $needle = get_string('modulename', 'prevaluation').':'.$instancename;
                    $needle = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $needle);
                    $stack = $headermapsto[get_string('gradeitems', 'grades')];
                    foreach ($stack as $key => $string) {
                        $result = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $string);
                        if($result == $needle)
                        {
                            if($activity_lock)
                            {
                                $mform->setDefault('mapping_'.$i, $key);
                            }
                            else
                            {
                                $mform->addElement('hidden', 'mapping_'.$i, $key);
                            }
                        }
                    }
                }
                $i++;
            }
        }


        // course id needs to be passed for auth purposes
        $mform->addElement('hidden', 'map', 1);
        $mform->setType('map', PARAM_INT);
        $mform->setConstant('map', 1);
        $mform->addElement('hidden', 'id', $this->_customdata['id']);
        $mform->setType('id', PARAM_INT);
        $mform->setConstant('id', $this->_customdata['id']);
        $mform->addElement('hidden', 'iid', $this->_customdata['iid']);
        $mform->setType('iid', PARAM_INT);
        $mform->setConstant('iid', $this->_customdata['iid']);
        $mform->addElement('hidden', 'importcode', $this->_customdata['importcode']);
        $mform->setType('importcode', PARAM_FILE);
        $mform->setConstant('importcode', $this->_customdata['importcode']);
        $mform->addElement('hidden', 'verbosescales', 1);
        $mform->setType('verbosescales', PARAM_INT);
        $mform->setConstant('verbosescales', $this->_customdata['verbosescales']);
        $mform->addElement('hidden', 'groupid', groups_get_course_group($COURSE));
        $mform->setType('groupid', PARAM_INT);
        $mform->setConstant('groupid', groups_get_course_group($COURSE));
        $mform->addElement('hidden', 'forceimport', $this->_customdata['forceimport']);
        $mform->setType('forceimport', PARAM_BOOL);
        $mform->setConstant('forceimport', $this->_customdata['forceimport']);
        $this->add_action_buttons(false, get_string('uploadgrades', 'grades'));

    }
}