<?php

/*
 * Plugin PREVALUATION dependency
 * Extension of moodleform
 * users view
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once 'locallib.php';
require_once $CFG->libdir.'/formslib.php';
require_once $CFG->libdir.'/gradelib.php';

global $PAGE;
$PAGE->requires->css("/mod/prevaluation/assets/prevaluation.css");

class prevaluation_users extends moodleform
{
    function definition (){
        global $COURSE, $DB, $PAGE, $CFG, $USER;

        $PAGE->requires->js("/mod/prevaluation/assets/prevaluation.js");
        
        $mformusers =& $this->_form;
        $mformusers->addElement('header', 'general', 'Studenti in attesa');//get_string('importfile', 'grades'));


        if (isset($this->_customdata)) {  // hardcoding plugin names here is hacky
            $features = $this->_customdata;
        } else {
            $features = array();
        }

        if (isset($_POST['form_action']))
        {
            switch ($_POST['form_action']) {
                case 'save':
                    foreach($_POST as $key => $value)
                    {
                        $exploded = explode('_',$key);
                        if(
                            $exploded[0] == 'user' &&
                            $exploded[2] == 'email'
                        )
                        {
                            $user_id = $exploded[1];
                            $data = array(
                                'user_id'       => $user_id,
                                'user_name'     => $_POST['user_'.$user_id.'_name'],
                                'user_surname'  => $_POST['user_'.$user_id.'_surname'],
                                'user_email'    => $_POST['user_'.$user_id.'_email'],
                                'user_grade'    => $_POST['user_'.$user_id.'_grade'],
                                'user_status'   => $_POST['user_'.$user_id.'_status']
                            );
                            prevaluation_save($data,$DB);
                        }
                    }
                    break;
                
                case 'delete':
                    foreach($_POST as $key => $value)
                    {
                        $exploded = explode('_',$key);
                        if(
                            $exploded[0] == 'user' &&
                            $exploded[2] == 'onoff' &&
                            $value == 'on'
                        )
                        {
                            $user_id = $exploded[1];
                            $data = array(
                                'user_id'       => $user_id,
                                'user_name'     => $_POST['user_'.$user_id.'_name'],
                                'user_surname'  => $_POST['user_'.$user_id.'_surname'],
                                'user_email'    => $_POST['user_'.$user_id.'_email'],
                                'user_grade'    => $_POST['user_'.$user_id.'_grade'],
                                'user_status'   => $_POST['user_'.$user_id.'_status']
                            );
                          prevaluation_delete($data,$DB);
                        }
                    }
                    break;
                
                case 'enroll':
                    foreach($_POST as $key => $value)
                    {
                        $exploded = explode('_',$key);
                        if(
                            $exploded[0] == 'user' &&
                            $exploded[2] == 'onoff' &&
                            $value == 'on'
                        )
                        {
                            $user_id = $exploded[1];
                            $data = array(
                                'user_id'       => $user_id,
                                'user_name'     => $_POST['user_'.$user_id.'_name'],
                                'user_surname'  => $_POST['user_'.$user_id.'_surname'],
                                'user_email'    => $_POST['user_'.$user_id.'_email'],
                                'user_grade'    => $_POST['user_'.$user_id.'_grade'],
                                'user_status'   => $_POST['user_'.$user_id.'_status'],
                                'user_courseid' => $COURSE->id,
                                //'activity_id'   => $_POST['activity_id']
                                'item_to_prevaluationuate'   => $_POST['item_to_prevaluationuate']
                            );

                            $moodle_user = $DB->get_record(
                                'user', 
                                array(
                                    'email'=> $_POST['user_'.$user_id.'_email']
                                )
                            );
                            if($moodle_user)
                            {
                                prevaluation_enroll($data,$DB);
                                prevaluation_grade($data,$DB);
                                prevaluation_delete($data,$DB);
                                //purge_all_caches();
                            }
                        }
                    }
                    purge_all_caches();
                    break;
                            
                default:
                    // nothing to do here
                    break;
            }

        }
        $waitingUsers = $DB->get_records(
            'prevaluation_waiting_users',
            array(
                'course_id'=> $features['id'],
                'instance_id' => $DB->get_record(
                    'course_modules',
                    array('id'=>$_GET['id'])
                )->instance
            )
        );
        
        if(count($waitingUsers) == 0 | $waitingUsers == null)
        {
            return;
        }

        $mformusers->addElement('html',
            '<p class="waiting-legend">
                <strong>Lista utenti che non hanno ancora ricevuto una valutazione perché non sono iscritti al corso</strong>
                <br>
                <br>
                <span><i class="icon fa fa-user" style="color:green;"></i> presente nella piattaforma</span>
                <span class="not-in-moodle"><i class="icon fa fa-user-times" style="color:red;"></i> non presente nella piattaforma</span>
                <span class="in-moodle user-with-errors"><i class="icon fa fa-user" style="color:orange;"></i><span style="color:orange">#</span> presente con errori</span>
                <!--<span class="enrolled">presente nella piattaforma ed iscritto al corso</span><br>-->
            </p>');

        $mformusers->addElement('html', '<table class="table table-condensed">');
        $mformusers->addElement('html', '<thead>');
        $mformusers->addElement('html', '<tr>');
        $mformusers->addElement('html', '<td>email</td>');
        $mformusers->addElement('html', '<td>nome</td>');
        $mformusers->addElement('html', '<td>cognome</td>');
        $mformusers->addElement('html', '<td>val.</td>');
        $mformusers->addElement('html', '<td></td>');
        $mformusers->addElement('html', '</tr>');
        $mformusers->addElement('html', '</thead>');

        foreach ($waitingUsers as $key => $user) {

            $moodle_user = $DB->get_record(
                'user', 
                array(
                    'email'=> $user->user_email
                )
            );

            $class = ( $moodle_user ? "in-moodle" : "not-in-moodle");

            if(($moodle_user !== false) & ($moodle_user->firstname !== $user->user_name | $moodle_user->lastname !== $user->user_surname))
            {
                $class .= " user-with-errors";
            }

            $mformusers->addElement('html', '<tr class="user '.$class.'">');
            $mformusers->addElement('html', '<td><input class="ajax-input" name="user_'.$user->id.'_email" type="text" value="'.$user->user_email.'"></td>');
            $mformusers->addElement('html', '<td><input class="ajax-input" name="user_'.$user->id.'_name" type="text" value="'.$user->user_name.'"></td>');
            $mformusers->addElement('html', '<td><input class="ajax-input" name="user_'.$user->id.'_surname" type="text" value="'.$user->user_surname.'"></td>');
            $mformusers->addElement('html', '<td><input class="ajax-input user_grade" name="user_'.$user->id.'_grade" type="text" value="'.$user->user_grade.'"></td>');
            $mformusers->addElement('html',
                '<td class="checkbox_control">
                    <div class="user-where-wrapper">
                    <span class="user-where">'.($moodle_user ? '<i class="icon fa fa-user"></i>' : '<i class="icon fa fa-user-times"></i>').'</span>
                    <input type="checkbox" name="user_'.$user->id.'_onoff">
                    <input hidden name="user_'.$user->id.'_courseid" value="'.$user->course_id.'">
                    <input hidden name="user_'.$user->id.'_status" value="'.$class.'">
                    </div>
                </td>'
            );
            $mformusers->addElement('html', '</tr>');
        }
        
        $mformusers->addElement('html', '<tr class="toggle_checkbox"><td colspan="4"><label for="toggle_internal"><small><i>'.get_string('toggle_internal', 'prevaluation').'</i></small></label></td><td><input type="checkbox" id="toggle_internal"></td></tr>');
        $mformusers->addElement('html', '<tr class="toggle_checkbox"><td colspan="4"><label for="toggle_external"><small><i>'.get_string('toggle_external', 'prevaluation').'</i></small></label></td><td><input type="checkbox" id="toggle_external"></td></tr>');

        $mformusers->addElement('html', '</table>');
        $mformusers->addElement('html','<input hidden name="form_action" id="form_action" value="-">');
        $options = array('-'=>'-', 'delete'=>'Cancella dalla lista', 'enroll'=>'Iscrivi e valuta');
        $mformusers->addElement('select', 'action', get_string('action','prevaluation'), $options);

        $assignments = $DB->get_records(
            'assign',
            array(
                'course' => $COURSE->id
            )
        );

        $activities = get_array_of_activities($COURSE->id);
        $options = array();
        
        $activity_lock = (get_config('prevaluation')->activity_lock) ? 1 : null;
        if($activity_lock)
        {
            foreach ($assignments as $key => $assignment) {
                $options['assignment_'.$assignment->id] = $assignment->name;
            }
            //$options['assignment_foo2'] = 'foo2';
            $options['activity_'.$activities[$_GET['id']]->id] = $activities[$_GET['id']]->name;
            //$options['assignment_foo'] = 'foo';            
            $mformusers->addElement('select', 'item_to_prevaluationuate', get_string('activity_list','prevaluation'), $options);
            //$this->_form->disabledIf('item_to_prevaluationuate',$activity_lock);
        }
        else
        {
            $mformusers->addElement('hidden', 'item_to_prevaluationuate', 'activity_'.$activities[$_GET['id']]->id);  
        }

        $action = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
        $mformusers->addElement('hidden', 'form_path', $action);  
        //$mformusers->addElement('hidden', 'form_action', $action);  
        //$mformusers->addElement('hidden', 'activity_id', $activities[$_GET['id']]->id);
        $mformusers->addElement('html', '<a class="btn btn-primary" id="pending_submit">Esegui azione</a>');
        $mformusers->addElement('html', '<div class="clearfix"></div>');
        $mformusers->addElement('html', '<a class="btn btn-primary" id="pending_save">Salva valutazioni</a>');
    }
}