<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Internal library of functions for module prevaluation
 *
 * All the prevaluation specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_prevaluation
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/*
 * Does something really useful with the passed things
 *
 * @param array $things
 * @return object
 *function prevaluation_do_something_useful(array $things) {
 *    return new stdClass();
 *}
 */

function prevaluation_save($data, $DB)
{
	$data = array(
		"id"		 	=> $data["user_id"],
		"user_email" 	=> $data["user_email"],
		"user_name"		=> $data["user_name"],
		"user_surname" 	=> $data["user_surname"],
		"user_grade" 	=> $data["user_grade"]
	);
	return $DB->update_record(
		"prevaluation_waiting_users",
		$data,
		$bulk=false
	);
}

function prevaluation_delete($data, $DB)
{
    return $DB->delete_records(
        'prevaluation_waiting_users',
        array(
            'id'=> $data["user_id"]
        )
    );
}

function prevaluation_enroll($data, $DB)
{
	$user_record_id = $data['user_id'];
	$user_status = $data['user_status'];
	$user_email = $data['user_email'];
	$courseid 	= $data['user_courseid'];
	$time = time();

	// skip if the student is not in moodle
	if ($user_status == 'not-in-moodle')
	{
		return $data['user_email'].' not in moodle';
	}

    $prevaluation_user = $DB->get_record(
        'prevaluation_waiting_users',
        array('id'=> $user_record_id)
    );

    $user = $DB->get_record(
    	'user',
    	array('id' => $prevaluation_user->user_id, 'deleted' => 0),
    	'*',
    	MUST_EXIST
   	);

    $userid = $user->id;
    $grade = $user->user_grade;
    $enrolmethod = 'manual';
    $roleid = 5;
    $context = context_course::instance($courseid);
    $enrolled = is_enrolled($context, $user);

    // skip if the student is already enrolled
    if(is_enrolled($context, $user) == true)
    {
    	return $data['user_email'].' already enrolled';
    }

    // the student can finally be enrolled
    $enrol = $DB->get_record(
        'enrol', 
        array(
            'courseid'=> $courseid,
            'enrol' => 'manual'
        )
    );
    $idenrol = $enrol->id;

    $context = $DB->get_record(
        'context', 
        array(
            'contextlevel' => 50,
            'instanceid' => $courseid
        )
    );
    $idcontext = $context->id;

    $params = array(
        'status' => 0,
        'enrolid' => $idenrol,
        'userid' => $userid,
        'timestart' => $time,
        'timeend' => 0,
        'timecreated' => $time,
        'timemodified' => $time
    );
    $new_enrolment = $DB->insert_record(
        'user_enrolments',
        $params
    );

    $new_role = $DB->insert_record(
        'role_assignments',
        array(
            'roleid' => 5,
            'contextid' => $idcontext,
            'userid' => $userid,
            'timemodified' => $time
        )
    );
	return $data['user_email'].' has been enrolled';
}

function prevaluation_grade($data, $DB)
{
    $exploded = explode('_',$data['item_to_prevaluationuate']);
    if($exploded[0] == 'assignment')
    {
        return prevaluation_grade_assignment($data,$DB,$exploded[1]);
    }
    if($exploded[0] == 'activity')
    {
        return prevaluation_grade_activity($data,$DB,$exploded[1]);
    }
}

function prevaluation_grade_activity($data,$DB,$activityid)
{

    $user_record_id = $data['user_id'];
    $user_status = $data['user_status'];
    $user_email = $data['user_email'];
    $courseid   = $data['user_courseid'];
    $grade      = $data['user_grade'];
    $time = time();

    /*
    echo '<pre>';
    var_dump($user_record_id);
    var_dump($user_status);
    var_dump($user_email);
    var_dump($courseid);
    var_dump($grade);
    var_dump($time);
    die('from observer');
    */

    // skip if the student is not in moodle
    if ($user_status !== 'not-in-moodle')
    {
        $prevaluation_user = $DB->get_record(
            'prevaluation_waiting_users',
            array('id'=> $user_record_id)
        );

        $user = $DB->get_record(
            'user',
            array('id' => $prevaluation_user->user_id, 'deleted' => 0),
            '*',
            MUST_EXIST
        );

        $userid = $user->id;

        
        // COMPLETION AND GRADE
        $itemid = $DB->get_record(
            'grade_items',
            array(
                'itemtype'=> 'mod',
                'itemmodule'=> 'prevaluation',//'assign',
                //'iteminstance' => $_POST['assignment']
                'iteminstance' => $activityid
            )
        )->id;

        // SET GRADE
        $oldgrade = $DB->get_record(
            'grade_grades',
            array(
                'itemid' => $itemid,
                'userid' => $userid
            )
        );   

        if($oldgrade)
        {
            $DB->update_record(
                'grade_grades',
                array(
                    'id' => $oldgrade->id,
                    'finalgrade' => $grade
                )
            );   
        }
        else
        {
            $DB->insert_record(
                'grade_grades',
                array(
                    'itemid' => $itemid,
                    'userid' => $userid,
                    'finalgrade' => $grade
                )
            ); 

            /*
            echo '<pre>';
            var_dump($itemid);
            var_dump($userid);
            var_dump($grade);
            die('insert grade');
            */
        }  

        // SET COMPLETION
        $mdl_module = $DB->get_record(
            "modules",
            array(
                "name" => 'prevaluation'
            )
        )->id;

        $moduleId = $DB->get_record(
            "course_modules",
            array(
                "course" => $courseid,
                "module" => $mdl_module,
                "instance" => $activityid
            )
        )->id;

        $recordid = $DB->get_record(
            'course_modules_completion',
            array(
                "coursemoduleid"    => $moduleId,
                "userid"            => $userid
            ),
            '*',
            IGNORE_MISSING
        )->id;

        $grade_item = $DB->get_record(
            'grade_items',
            array(
                "itemmodule"        => 'prevaluation',
                "iteminstance"      => $activityid
            ),
            '*',
            IGNORE_MISSING
        );

        // 0 for no completion
        // 1 for blu flag
        // 2 for green flag
        // 3 for red cross

        if($grade_item->gradepass)
        {
            $gradepass = floatval($grade_item->gradepass);
            $state = $grade >= $gradepass ? 2 : 3;
        }
        else
        {
            $gradepass = 0;
            $state = 1;
        }

        if ($recordid !== null)
        {
            $DB->update_record(
                'course_modules_completion',
                array(
                    "id"                => $recordid,
                    "coursemoduleid"    => $moduleId,
                    "userid"            => $userid,
                    "completionstate"   => $state,
                    "viewed"            => 0,
                    "timemodified"      => $time
                ),
                false
            );
        }
        else
        {
            $recordid = $DB->insert_record(
                'course_modules_completion',
                array(
                    "coursemoduleid"    => $moduleId,
                    "userid"            => $userid,
                    "completionstate"   => $state,
                    "viewed"            => 0,
                    "timemodified"      => $time
                ),
                true,   //return id
                false   //docs just says this parameter is unused (??)
            );
        }
    }

    //return $data['user_email'].' has been graded';
}

function prevaluation_grade_assignment($data,$DB,$assignmentid)
{
    //die('trying to prevaluationuate ASSIGNMENT: '.$data['user_email']);

    $user_record_id = $data['user_id'];
    $user_status = $data['user_status'];
    $user_email = $data['user_email'];
    $courseid   = $data['user_courseid'];
    $grade      = $data['user_grade'];
    $time       = time();

    // skip if the student is not in moodle
    if ($user_status !== 'in-moodle')
    {
        return;// $data['user_email'].' not in moodle';
    }

    $prevaluation_user = $DB->get_record(
        'prevaluation_waiting_users',
        array('id'=> $user_record_id)
    );

    $user = $DB->get_record(
        'user',
        array('id' => $prevaluation_user->user_id, 'deleted' => 0),
        '*',
        MUST_EXIST
    );

    $userid = $user->id;


    //public static function set_assignment_grade($grade, $userId, $assignmentId, $timestamp)
    //{
        //global $DB;

        $record = $DB->get_record(
            'assign_grades',
            array(
                "userid"        => $userid,
                "assignment"    => $assignmentid,
            ),
            '*',
            IGNORE_MISSING
        );

        $recordid = $record->id;

        if ($recordid !== null)
        {
            $DB->update_record(
                'assign_grades',
                array(
                    "id"            => $recordid,
                    "userid"        => $userid,
                    "grader"        => $userid,
                    "assignment"    => $assignmentid,
                    //"timecreated"     => $time,
                    "timemodified"  => $time,
                    "grade"         => $grade
                ),
                false
            );
        }
        else        
        {
            $recordid = $DB->insert_record(
                'assign_grades',
                array(
                    "userid"        => $userid,
                    "grader"        => $userid,
                    "assignment"    => $assignmentid,
                    "timecreated"   => $time,
                    "timemodified"  => $time,
                    "grade"         => $grade
                ),
                true,   //return id
                false   //docs just says this parameter is unused (??)
            );
        }

        if ($recordid == false)
        {
            return false;
        }

        //die('user graded! manca il completamento.....');




        // SET COMPLETION
        $mdl_module = $DB->get_record(
            "modules",
            array(
                "name" => 'assign'
            )
        )->id;

        $moduleId = $DB->get_record(
            "course_modules",
            array(
                "course" => $courseid,
                "module" => $mdl_module,
                "instance" => $assignmentid
            )
        )->id;

        $record = $DB->get_record(
            'course_modules_completion',
            array(
                "coursemoduleid"    => $moduleId,
                "userid"            => $userid
            ),
            '*',
            IGNORE_MISSING
        );

        $id = $record->id;

        $grade_item = $DB->get_record(
            'grade_items',
            array(
                "itemmodule"        => 'assign',
                "iteminstance"      => $assignmentid
            ),
            '*',
            IGNORE_MISSING
        );

        // 0 for no completion
        // 1 for blu flag
        // 2 for green flag
        // 3 for red cross

        if($grade_item->gradepass)
        {
            $gradepass = floatval($grade_item->gradepass);
            $state = $grade >= $gradepass ? 2 : 3;
        }
        else
        {
            $gradepass = 0;
            $state = 1;
        }

        if ($id !== null)
        {
            $DB->update_record(
                'course_modules_completion',
                array(
                    "id"                => $id,
                    "coursemoduleid"    => $moduleId,
                    "userid"            => $userid,
                    "completionstate"   => $state,
                    "viewed"            => 0,
                    "timemodified"      => $time
                ),
                false
            );
        }
        else
        {
            $id = $DB->insert_record(
                'course_modules_completion',
                array(
                    "coursemoduleid"    => $moduleId,
                    "userid"            => $userid,
                    "completionstate"   => $state,
                    "viewed"            => 0,
                    "timemodified"      => $time
                ),
                true,   //return id
                false   //docs just says this parameter is unused (??)
            );

        }

        return true;
    //}

}



// utilizzato solo nel csv
function prevaluation_direct_grade_activity($data,$DB,$activityid)
{
    $userid = $data['user_id'];
    $user_status = $data['user_status'];
    $user_email = $data['user_email'];
    $courseid   = $data['user_courseid'];
    $grade      = $data['user_grade'];
    $time = time();

    // skip if the student is not in moodle
    if ($user_status !== 'in-moodle')
    {
        return;// $data['user_email'].' not in moodle';
    }
    
    // COMPLETION AND GRADE
    $itemid = $DB->get_record(
        'grade_items',
        array(
            'itemtype'=> 'mod',
            'itemmodule'=> 'prevaluation',//'assign',
            //'iteminstance' => $_POST['assignment']
            'iteminstance' => $activityid
        )
    )->id;

    // SET GRADE
    $oldgrade = $DB->get_record(
        'grade_grades',
        array(
            'itemid' => $itemid,
            'userid' => $userid
        )
    );   

    if($oldgrade)
    {
        $DB->update_record(
            'grade_grades',
            array(
                'id' => $oldgrade->id,
                'finalgrade' => $grade
            )
        );   
    }
    else
    {
        $DB->insert_record(
            'grade_grades',
            array(
                'itemid' => $itemid,
                'userid' => $userid,
                'finalgrade' => $grade
            )
        ); 
    }  

    // SET COMPLETION
    $mdl_module = $DB->get_record(
        "modules",
        array(
            "name" => 'prevaluation'
        )
    )->id;

    $moduleId = $DB->get_record(
        "course_modules",
        array(
            "course" => $courseid,
            "module" => $mdl_module,
            "instance" => $activityid
        )
    )->id;

    $recordid = $DB->get_record(
        'course_modules_completion',
        array(
            "coursemoduleid"    => $moduleId,
            "userid"            => $userid
        ),
        '*',
        IGNORE_MISSING
    )->id;

    $grade_item = $DB->get_record(
        'grade_items',
        array(
            "itemmodule"        => 'prevaluation',
            "iteminstance"      => $activityid
        ),
        '*',
        IGNORE_MISSING
    );

    // 0 for no completion
    // 1 for blu flag
    // 2 for green flag
    // 3 for red cross

    if($grade_item->gradepass)
    {
        $gradepass = floatval($grade_item->gradepass);
        $state = $grade >= $gradepass ? 2 : 3;
    }
    else
    {
        $gradepass = 0;
        $state = 1;
    }

    if ($recordid !== null)
    {
        $DB->update_record(
            'course_modules_completion',
            array(
                "id"                => $recordid,
                "coursemoduleid"    => $moduleId,
                "userid"            => $userid,
                "completionstate"   => $state,
                "viewed"            => 0,
                "timemodified"      => $time
            ),
            false
        );
    }
    else
    {
        $recordid = $DB->insert_record(
            'course_modules_completion',
            array(
                "coursemoduleid"    => $moduleId,
                "userid"            => $userid,
                "completionstate"   => $state,
                "viewed"            => 0,
                "timemodified"      => $time
            ),
            true,   //return id
            false   //docs just says this parameter is unused (??)
        );

    }

    return $data['user_email'].' has been graded';
}


// utilizzato solo nel csv
function prevaluation_remove_grade_activity($data,$DB,$activityid)
{
    //echo '<pre>';
    //var_dump($data);
    //die('test');
    $userid = $data['user_id'];
    $user_status = $data['user_status'];
    $user_email = $data['user_email'];
    $courseid   = $data['user_courseid'];

    // skip if the student is not in moodle
    if ($user_status !== 'in-moodle')
    {
        return;// $data['user_email'].' not in moodle';
    }
    
    // REMOVE GRADE
    $itemid = $DB->get_record(
        'grade_items',
        array(
            'itemtype'=> 'mod',
            'itemmodule'=> 'prevaluation',
            'iteminstance' => $activityid
        )
    )->id;

    $DB->delete_records(
        'grade_grades',
        array(
            'itemid' => $itemid,
            'userid' => $userid
        )
    );

    // REMOVE COMPLETION
    $mdl_module = $DB->get_record(
        "modules",
        array(
            "name" => 'prevaluation'
        )
    )->id;

    $moduleId = $DB->get_record(
        "course_modules",
        array(
            "course" => $courseid,
            "module" => $mdl_module,
            "instance" => $activityid
        )
    )->id;

    $DB->delete_records(
        'course_modules_completion',
        array(
            "coursemoduleid"    => $moduleId,
            "userid"            => $userid
        )
    );    

    /*
    $recordid = $DB->get_record(
        'course_modules_completion',
        array(
            "coursemoduleid"    => $moduleId,
            "userid"            => $userid
        ),
        '*',
        IGNORE_MISSING
    )->id;

    $grade_item = $DB->get_record(
        'grade_items',
        array(
            "itemmodule"        => 'prevaluation',
            "iteminstance"      => $activityid
        ),
        '*',
        IGNORE_MISSING
    );

    // 0 for no completion
    // 1 for blu flag
    // 2 for green flag
    // 3 for red cross

    if($grade_item->gradepass)
    {
        $gradepass = floatval($grade_item->gradepass);
        $state = $grade >= $gradepass ? 2 : 3;
    }
    else
    {
        $gradepass = 0;
        $state = 1;
    }

    if ($recordid !== null)
    {
        $DB->update_record(
            'course_modules_completion',
            array(
                "id"                => $recordid,
                "coursemoduleid"    => $moduleId,
                "userid"            => $userid,
                "completionstate"   => $state,
                "viewed"            => 0,
                "timemodified"      => $time
            ),
            false
        );
    }
    else
    {
        $recordid = $DB->insert_record(
            'course_modules_completion',
            array(
                "coursemoduleid"    => $moduleId,
                "userid"            => $userid,
                "completionstate"   => $state,
                "viewed"            => 0,
                "timemodified"      => $time
            ),
            true,   //return id
            false   //docs just says this parameter is unused (??)
        );

    }

    return $data['user_email'].' has been graded';
    */
}







/////
//function duplicate_module($course, $cm) {
function duplicate_instance($course, $cm) {

    global $CFG;

    header("location: $CFG->wwwroot/course/course/view.php?id=".$_GET["id"]);


    /*
    global $CFG, $DB, $USER;
    require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
    require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
    require_once($CFG->libdir . '/filelib.php');

    $a          = new stdClass();
    $a->modtype = get_string('modulename', $cm->modname);
    $a->modname = format_string($cm->name);

    if (!plugin_supports('mod', $cm->modname, FEATURE_BACKUP_MOODLE2)) {
        throw new moodle_exception('duplicatenosupport', 'error', '', $a);
    }

    // Backup the activity.

    $bc = new backup_controller(backup::TYPE_1ACTIVITY, $cm->id, backup::FORMAT_MOODLE,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id);

    $backupid       = $bc->get_backupid();
    $backupbasepath = $bc->get_plan()->get_basepath();

    $bc->execute_plan();

    $bc->destroy();

    // Restore the backup immediately.

    $rc = new restore_controller($backupid, $course->id,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id, backup::TARGET_CURRENT_ADDING);

    $cmcontext = context_module::instance($cm->id);
    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
            if (empty($CFG->keeptempdirectoriesonbackup)) {
                fulldelete($backupbasepath);
            }
        }
    }

    $rc->execute_plan();

    // Now a bit hacky part follows - we try to get the cmid of the newly
    // restored copy of the module.
    $newcmid = null;
    $tasks = $rc->get_plan()->get_tasks();
    foreach ($tasks as $task) {
        if (is_subclass_of($task, 'restore_activity_task')) {
            if ($task->get_old_contextid() == $cmcontext->id) {
                $newcmid = $task->get_moduleid();
                break;
            }
        }
    }

    $rc->destroy();

    if (empty($CFG->keeptempdirectoriesonbackup)) {
        fulldelete($backupbasepath);
    }

    // If we know the cmid of the new course module, let us move it
    // right below the original one. otherwise it will stay at the
    // end of the section.
    if ($newcmid) {
        $section = $DB->get_record('course_sections', array('id' => $cm->section, 'course' => $cm->course));
        $modarray = explode(",", trim($section->sequence));
        $cmindex = array_search($cm->id, $modarray);
        if ($cmindex !== false && $cmindex < count($modarray) - 1) {
            $newcm = get_coursemodule_from_id($cm->modname, $newcmid, $cm->course);
            moveto_module($newcm, $section, $modarray[$cmindex + 1]);
        }

        // Update calendar events with the duplicated module.
        // The following line is to be removed in MDL-58906.
        course_module_update_calendar_events($newcm->modname, null, $newcm);

        // Trigger course module created event. We can trigger the event only if we know the newcmid.
        $newcm = get_fast_modinfo($cm->course)->get_cm($newcmid);
        $event = \core\event\course_module_created::create_from_cm($newcm);
        $event->trigger();
    }

    return isset($newcm) ? $newcm : null;
    */
}
