<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the restore steps that will be used by the restore_prevaluation_activity_task
 *
 * @package   mod_prevaluation
 * @category  backup
 * @copyright 2016 Your Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Structure step to restore one prevaluation activity
 *
 * @package   mod_prevaluation
 * @category  backup
 * @copyright 2016 Your Name <your@email.address>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_prevaluation_activity_structure_step extends restore_activity_structure_step {

    /**
     * Defines structure of path elements to be processed during the restore
     *
     * @return array of {@link restore_path_element}
     */
    protected function define_structure() {

        $paths = array();
        $paths[] = new restore_path_element('prevaluation', '/activity/prevaluation');

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    /**
     * Process the given restore path element data
     *
     * @param array $data parsed element data
     */
    protected function process_prevaluation($data) {
        global $DB, $CFG;
            
        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        if (empty($data->timecreated)) {
            $data->timecreated = time();
        }

        if (empty($data->timemodified)) {
            $data->timemodified = time();
        }

        if ($data->grade < 0) {
            // Scale found, get mapping.
            $data->grade = -($this->get_mappingid('scale', abs($data->grade)));
        }


        // Hack to get if this restore is part of duplicate action.
        //$duplicate = false;
        //$backtraces = debug_backtrace();
        //file_put_contents($CFG->dataroot."/preval_backtrace.txt", print_r($backtraces,true));
        //foreach ($backtraces as $i => $backtrace) {
        //    if ($backtrace['function'] == 'duplicate_module') {
        //        $duplicate = true;
        //    }
        //}
        //if($duplicate)
        {
            $originalinstance = $DB->get_record(
                "prevaluation",
                array(
                    "id" => $data->id
                )
            );
            $data->showdetails = $originalinstance->showdetails;
            $data->msg_1 = $originalinstance->msg_1;
            $data->msg_2 = $originalinstance->msg_2;
            $data->msg_3 = $originalinstance->msg_3;
            $data->csv_separator = $originalinstance->csv_separator;
            $data->index_firstname = $originalinstance->index_firstname;
            $data->index_lastname = $originalinstance->index_lastname;
            $data->index_email = $originalinstance->index_email;
            $data->index_grade = $originalinstance->index_grade;
        }
        // End hack to get if this restore is part of duplicate action.


        // Create the prevaluation instance.
        $newitemid = $DB->insert_record('prevaluation', $data);

        $this->apply_activity_instance($newitemid);

    }

    /**
     * Post-execution actions
     */
    protected function after_execute() {
        // Add prevaluation related files, no need to match by itemname (just internally handled context).
        $this->add_related_files('mod_prevaluation', 'intro', null);
    }
}
