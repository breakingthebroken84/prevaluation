/*
window.onbeforeunload = function(e) {
  // Cancel the event
  e.preventDefault();
  // Chrome requires returnValue to be set
  e.returnValue = '';
};
*/


/*
function saveUser(user, path)
{
	var path = path+'?user_grade='+user.user_grade+'&course_id='+user.course_id+'&user_name='+user.user_name+'&user_surname='+user.user_surname+'&user_email='+user.user_email+'&user_id='+user.user_id+'&record_id='+user.record_id;
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(data) {
		if (xhr.readyState == XMLHttpRequest.DONE) {
		   	if (xhr.status == 200) {
		   		//console.log(xhr.responseText);
		   		console.log('save ok');


				//box.classList.remove("wait");
				//box.classList.add("done");
				//window.setTimeout(function(){
				//	box.classList.remove("done");
				//},2000);
			}
			else
			{
				//console.log(xhr.responseText);
				console.log('save ko');

				//box.classList.remove("wait");
				//box.classList.add("bad");
				//window.setTimeout(function(){
				//	box.classList.remove("bad");
				//},2000);
		  	}
		}
	};
	xhr.open("GET", path, true);
	xhr.send();
}
*/

var toggle_internal = document.getElementById('toggle_internal');
if (toggle_internal !== null)
{
	toggle_internal.addEventListener("click",function(e){
		var toggle = e.target.checked;
		if(toggle)
		{
			var c = document.querySelectorAll('.in-moodle input[type="checkbox"]');
			for (var i = c.length - 1; i >= 0; i--) {
				c[i].checked = true;
			}
		}
		else
		{
			var c = document.querySelectorAll('.in-moodle input[type="checkbox"]');
			for (var i = c.length - 1; i >= 0; i--) {
				c[i].checked = false;
			}
		}
	});
}

var toggle_external = document.getElementById('toggle_external');
if (toggle_external !== null)
{
	toggle_external.addEventListener("click",function(e){
		var toggle = e.target.checked;
		if(toggle)
		{
			var c = document.querySelectorAll('.not-in-moodle input[type="checkbox"]');
			for (var i = c.length - 1; i >= 0; i--) {
				c[i].checked = true;
			}
		}
		else
		{
			var c = document.querySelectorAll('.not-in-moodle input[type="checkbox"]');
			for (var i = c.length - 1; i >= 0; i--) {
				c[i].checked = false;
			}
		}
	});
}

var pending_save = document.getElementById('pending_save');
if (pending_save !== null)
{
	pending_save.addEventListener("click",function(e){
		//console.log('clicked save');
		window.onbeforeunload = null;
		var form = document.querySelector('[id^="mform2"]'),
			field = document.getElementById('form_action');
		field.value = 'save';
		form.submit();
		return false;
	});
}


var pending_submit = document.getElementById('pending_submit');
if(pending_submit !== null)
{
	pending_submit.addEventListener("click",function(e){
		//console.log('clicked submit');
		window.onbeforeunload = null;
		var action = document.getElementById('id_action').value,
			form = document.querySelector('[id^="mform2"]'),
			field = document.getElementById('form_action');
		if (action !== '-')
		{
			field.value = action;
			form.submit();
		}
	});
}


/*
var save_showdetails = document.getElementById('id_showdetails');
if(save_showdetails !== null)
{
	save_showdetails.addEventListener("click",function(e){
		save_showdetails.value = (save_showdetails.checked ? 1 : 0);// = !e.value;
	});
}
*/

document.addEventListener("DOMContentLoaded", function(event) {
	var form = document.querySelector('[id^="mform2"]'),
		path = document.querySelectorAll('[name=form_path]');

	if(path !== null && form !== null)
	{
		path = path[0].value;
		form.setAttribute('action',path);
	}
/*
	var users = document.getElementsByClassName('ajax-input');
	window.timeout = null;
	window.element = null;
	for (var i = 0; i < users.length; ++i) {
	    var user = users[i];

		// Listen for keystroke events
		user.onkeyup = function (e) {
		    clearTimeout(window.timeout);
		    window.element = this;
		    timeout = setTimeout(function () {
		        var user = window.element.parentNode.parentNode,
		        	mod = user.getElementsByClassName('modified')[0]
		        	;
		        	mod.value = "1";
		    }, 250);
		};
	}
*/
});

/*
document.addEventListener("DOMContentLoaded", function(event) {
	var users = document.getElementsByClassName('ajax-input');
	window.timeout = null;
	window.element = null;
	for (var i = 0; i < users.length; ++i) {
	    var user = users[i];

		// Listen for keystroke events
		user.onkeyup = function (e) {
		    clearTimeout(window.timeout);
		    window.element = this;
		    timeout = setTimeout(function () {
		        //console.log(window.element.value);
		        var user = window.element.parentNode.parentNode;
		        var user_email = user.getElementsByClassName('user_email')[0].value;
		        saveUser({
		        	user_name : user.getElementsByClassName('user_name')[0].value,
		        	user_surname : user.getElementsByClassName('user_surname')[0].value,
		        	user_email : user.getElementsByClassName('user_email')[0].value,
		        	user_id : user.getElementsByClassName('user_id')[0].value,
		        	record_id : user.getElementsByClassName('record_id')[0].value,
		        	course_id : user.getElementsByClassName('course_id')[0].value,
		        	user_grade : user.getElementsByClassName('user_grade')[0].value
		        }, user.getElementsByClassName('moodle_path')[0].value);
		        //console.log(user_email);
		    }, 1000);
		};
	}

	var ajax_submit = document.getElementById('ajax-submit');

	ajax_submit.addEventListener("click",function(){
		var action = document.getElementById('id_action').value;
		console.log(action);
		return false;
	});

});
*/


