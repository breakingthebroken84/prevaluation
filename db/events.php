<?php

defined('MOODLE_INTERNAL') || die();

    $observers = array(
        /*
        array(
            'eventname' => '\core\event\course_deleted',
            'callback' => 'mod_prevaluation_observer::course_deleted',
        ),
        */
        array(
            'eventname' => 'core\event\user_enrolment_created',
            'callback' => 'mod_prevaluation_observer::user_enrolment_created',
        ),
        /*,
        array(
            'eventname' => 'core\event\user_enrolment_deleted',
            'callback' => 'mod_prevaluation_observer::user_unenrolled',
        ),*/
    );
