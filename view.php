<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of prevaluation
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_prevaluation
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

//require_once("../../../config.php");
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->dirroot.'/grade/lib.php');
require_once($CFG->dirroot. '/grade/import/grade_import_form.php');
require_once($CFG->dirroot.'/grade/import/lib.php');
require_once($CFG->libdir . '/csvlib.class.php');

require_once(dirname(__FILE__).'/prevaluation_import_csv_load_data.php');

//require_once(dirname(__FILE__).'/prevaluation_import_form.php');
require_once(dirname(__FILE__).'/prevaluation_users_form.php');
require_once(dirname(__FILE__).'/prevaluation_users_student_view.php');
require_once(dirname(__FILE__).'/prevaluation_import_form.php');
require_once(dirname(__FILE__).'/prevaluation_import_mapping_form.php');

GLOBAL $DB;
GLOBAL $PAGE;
GLOBAL $USER;
GLOBAL $SESSION;


$id            = required_param('id', PARAM_INT); // Course id.
$separator     = optional_param('separator', '', PARAM_ALPHA);
$verbosescales = optional_param('verbosescales', 1, PARAM_BOOL);
$iid           = optional_param('iid', null, PARAM_INT);
$importcode    = optional_param('importcode', '', PARAM_FILE);
$forceimport   = optional_param('forceimport', false, PARAM_BOOL);

$parts = parse_url($SESSION->fromdiscussion);
parse_str($parts['query'], $query);
$id = $query['id'];
$courseModule = $DB->get_record('course_modules', array('id'=>$moduleId));

$url = new moodle_url('/mod/prevaluation/index.php', array('id'=>$id));
if ($separator !== '') {
    $url->param('separator', $separator);
}
if ($verbosescales !== 1) {
    $url->param('verbosescales', $verbosescales);
}
$PAGE->set_url($url);

if (!$course = $DB->get_record('course', array('id'=>$id))) {
    print_error('invalidcourseid');
}

require_login($course);
$context = context_course::instance($id);

if(
    has_capability('moodle/grade:import', $context) == true &&
    has_capability('gradeimport/csv:view', $context) == true
)
{
    require_capability('moodle/grade:import', $context);
    require_capability('gradeimport/csv:view', $context);

    $separatemode = (groups_get_course_groupmode($COURSE) == SEPARATEGROUPS and !has_capability('moodle/site:accessallgroups', $context));
    $currentgroup = groups_get_course_group($course);

    print_grade_page_head(
        $course->id,                        //$courseid
        'import',                           //$active_type
        'csv',                              //$active_plugin=null
        get_string('importcsv','grades'),   //$heading = false
        false,                              //$return=false
        false,                              //$buttons=false
        false,                              //$shownavigation=true
        'importcsv',                        //$headerhelpidentifier = null
        'grades'                            //$headerhelpcomponent = null
                                            //$user = null
    );

    $renderer = $PAGE->get_renderer('gradeimport_csv');

    // Get the grade items to be matched with the import mapping columns.
    $gradeitems = gradeimport_csv_load_data::fetch_grade_items($course->id);

    // If the csv file hasn't been imported yet then look for a form submission or
    // show the initial submission form.
    if (!$iid) {

        // Set up the import form.
        //$mform = new grade_import_form(null, array('includeseparator' => true, 'verbosescales' => $verbosescales, 'acceptedtypes' => array('.csv', '.txt')));
        $mform = new prevaluation_import_form(
            null,
            array(
                'includeseparator' => true,
                'verbosescales' => false,
                'acceptedtypes' => array('.csv'),
                'instanceid' => $DB->get_record('course_modules', array('id'=>$_GET['id']))->instance
            )
        );
        $mformusers = new prevaluation_users(null,array("id"=>$id));

        // If the import form has been submitted.
        if ($formdata = $mform->get_data()) {
            $text = $mform->get_file_content('userfile');
            $csvimport = new gradeimport_csv_load_data();
            $csvimport->load_csv_content($text, $formdata->encoding, $separator, $formdata->previewrows);
            $csvimporterror = $csvimport->get_error();
            if (!empty($csvimporterror)) {
                echo $renderer->errors(array($csvimport->get_error()));
                echo $OUTPUT->footer();
                die();
            }
            $iid = $csvimport->get_iid();
            echo $renderer->import_preview_page($csvimport->get_headers(), $csvimport->get_previewdata());
        } else {
            // Display the standard upload file form.
            echo $renderer->standard_upload_file_form($course, $mform);
            echo $renderer->standard_upload_file_form($course, $mformusers);
            echo $OUTPUT->footer();
            die();
        }
    }

    // Data has already been submitted so we can use the $iid to retrieve it.
    $csvimport = new csv_import_reader($iid, 'grade');
    $header = $csvimport->get_columns();
    // Get a new import code for updating to the grade book.
    if (empty($importcode)) {
        $importcode = get_new_importcode();
    }

    $mappingformdata = array(
        'gradeitems' => $gradeitems,
        'header' => $header,
        'iid' => $iid,
        'id' => $id,
        'importcode' => $importcode,
        'forceimport' => $forceimport,
        'verbosescales' => $verbosescales
    );
    // we create a form to handle mapping data from the file to the database.
    $mform2 = new prevaluation_import_mapping_form(null, $mappingformdata);

    // Here, if we have data, we process the fields and enter the information into the database.
    if ($formdata = $mform2->get_data()) {
        $gradeimport = new prevaluation_import_csv_load_data();
        $status = $gradeimport->prepare_import_grade_data($header, $formdata, $csvimport, $course->id, $separatemode, $currentgroup, $verbosescales);

        // At this stage if things are all ok, we commit the changes from temp table.
        if ($status) {
            grade_import_commit($course->id, $importcode);
        } else {
            $errors = $gradeimport->get_gradebookerrors();
            $errors[] = get_string('importfailed', 'grades');
            echo $renderer->errors($errors);
        }
        echo $OUTPUT->footer();
    } else {
        // If data hasn't been submitted then display the data mapping form.
        $mform2->display();
        echo $OUTPUT->footer();
    }

}

else
{
    echo $OUTPUT->header();

    $moduleid = $DB->get_record (
                'course_modules',
                array(
                    'id' => $_GET['id']
                )
            )->instance;

    $instance_config = $DB->get_record(
        'prevaluation',
        array(
            'id' => $moduleid
        )
    );

    $instanceid = $instance_config->id;
    $msg_1 = $instance_config->msg_1;
    $msg_2 = $instance_config->msg_2;
    $msg_3 = $instance_config->msg_3;

    $waiting_records = $DB->get_records(
        'prevaluation_waiting_users',
        array(
            'instance_id' => $instanceid
        )
    );
    $itemid = $DB->get_record(
        'grade_items',
        array(
            'itemmodule' => 'prevaluation',
            'iteminstance' => $instanceid
        )
    )->id;
    $grade_records = $DB->get_records(
        'grade_grades',
        array(
            'itemid' => $itemid
        )
    );

    $waiting = count($waiting_records);
    $grades = count($grade_records);

    echo '<h3>'.get_string('info_students_title','prevaluation').'</h3>';

    if (($waiting + $grades) == 0)
    {
        echo '<p>'.$msg_1.'</p>';
    }
    else
    {
        if ($instance_config->showdetails)
        {
            //Mostra msg_2 CON la valutazione numerica per questo studente
            $grade_record = $DB->get_record(
                'grade_grades',
                array(
                    'itemid' => $itemid,
                    'userid' => $USER->id
                )
            );

            if($grade_record)
            {
                echo '<p>'.$msg_2.' '.intval($grade_record->finalgrade).'/'.intval($grade_record->rawgrademax).'</p>';
            }
            else
            {
                echo '<p>'.get_string('not_graded','prevaluation').'</p>';
            }

            // OLD
            // $formstudent = new prevaluation_users_student_view(null,array("id"=>$id));
            // $formstudent->display();
        }
        else
        {
            //Mostra msg_3 SENZA la valutazione numerica per questo studente
            echo '<p>'.$msg_3.'</p>';
        }
    }

    echo $OUTPUT->footer();
}
