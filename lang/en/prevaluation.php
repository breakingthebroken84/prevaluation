<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for prevaluation
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_prevaluation
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Prevaluation';
$string['modulenameplural'] = 'Prevaluations';
$string['modulename_help'] = 'Use the Prevaluation module for... | The Prevaluation module allows...';
$string['mapfrom'] = 'Mappa da (campo CSV)';
$string['mapto'] = 'Mappa a (campo Moodle)';
$string['prevaluation:addinstance'] = 'Add a new Prevaluation';
$string['prevaluation:submit'] = 'Submit Prevaluation';
$string['prevaluation:view'] = 'View Prevaluation';
$string['prevaluationfieldset'] = 'Custom example fieldset';
$string['settings'] = 'Impostazioni';
$string['prevaluationname'] = 'Prevaluation name';
$string['prevaluationname_help'] = 'This is the content of the help tooltip associated with the prevaluationname field. Markdown syntax is supported.';
$string['prevaluation'] = 'Prevaluation';
$string['pluginadministration'] = 'Prevaluation administration';
$string['pluginname'] = 'Prevaluation';
$string['toggle_internal'] = 'seleziona/deseleziona interni';
$string['toggle_external'] = 'seleziona/deseleziona esterni';
$string['enrolling_users'] = 'Studenti iscritti ed in attesa';
$string['info_students_title'] = 'Stato attività';
$string['graded'] = 'valutato';
$string['pending_external'] = 'in attesa (interno)';
$string['pending_internal'] = 'in attesa (esterno)';
//$string['prevaluation_no_users'] = 'Non ci sono utenti in attesa per questo corso.';
$string['show_more'] = 'Mostra';
$string['show_less'] = 'Non mostrare';
$string['show_info'] = 'Mostra informazioni agli utenti';
$string['show_info_help'] = 'Seleziona questa opzione per mostrare agli studenti anche il valore numerico delle proprie valutazioni';
$string['action'] = 'Azione';
$string['activity_list'] = 'Attività da valutare';

$string['not_graded'] = 'Non hai ricevuto valutazioni per questa attività';
$string['msg_1_label'] = 'Nessuna valutazione trovata';
$string['msg_2_label'] = 'Valutazione numerica';
$string['msg_3_label'] = 'Valutazione testuale';
$string['legend_id'] = 'Campo identificatore obbligatorio';
$string['legend_grade'] = 'Valutazione numerica obbligatoria';

//settings
$string['separator'] = 'Separatore';
$string['separator_desc'] = 'Imposta il separatore di default per i campi del CSV';
$string['comma'] = 'Virgola';
$string['dot'] = 'Punto';
$string['two_dots'] = 'Due punti';
$string['dot_and_comma'] = 'Punto e virgola';
$string['tab'] = 'Tabulatore';
$string['csv_mapping'] = 'Mappatura campi CSV';
$string['activity_lock'] = 'Valutazione altre attività';
$string['activity_lock_desc'] = 'Consenti di valutare altre attività oltre se stessa';

