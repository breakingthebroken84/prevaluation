<?php

global $CFG;

require_once $CFG->dirroot."/mod/prevaluation/locallib.php";

class mod_prevaluation_observer
{
    /**
     * Observer for the even user_enrolment_created:
     * - grades the user
     * - removes the user from the waiting user list
     * - purge caches
     *
     * @param \core\event\course_content_deleted $event
     */
	public static function user_enrolment_created(core\event\user_enrolment_created $event)
	{
		global $DB, $CFG;

		$event_data = $event->get_data();
		$user_id = $event_data["relateduserid"];
		$course_id = $event_data["courseid"];

		// userid should always be the performer of the action (teacher or administrator) while
		// relateduserid should be the subject of the action (student)
		if($user_id > 0)
		{
			$waiting = $DB->get_records(
				"prevaluation_waiting_users",
				array(
					"user_id" => $user_id,
					"course_id" => $course_id
				)
			);
			foreach ($waiting as $key => $record) {
				$activityid = intval($record->instance_id);
				$data = array(
				    "user_id"			=> $record->id,
				    "user_status"		=> "in-moodle",
				    "user_email"		=> $record->user_email,
				    "user_courseid"		=> $record->course_id,
				    "user_grade"		=> $record->user_grade
				);
				prevaluation_grade_activity($data,$DB,$activityid);
				prevaluation_delete($data,$DB);
			}
			purge_all_caches();
		}

    }

    /*
    public static function course_deleted(\core\event\course_deleted $event)
    {
		// debug event data ona text file log
		//file_put_contents($CFG->dataroot."/debug.txt", print_r($event,true));
        //file_put_contents($CFG->dataroot."/debug_data.txt", print_r($event_data,true));
        $event_data = $event->get_data();
        echo '<pre>';
        var_dump($event_data);
        die();
        echo '</pre>';
    }    
    */
}
