<?php

/*
 * Plugin PREVALUATION dependency
 * Extension of build in gradeimport_csv_load_data from /grade/import/grade_import_form.php
 */

//require_once(dirname(dirname(dirname(__FILE__))).'/grade/import/grade_import_form.php');
require_once $CFG->dirroot.'/grade/import/grade_import_form.php';
require_once 'locallib.php';

defined('MOODLE_INTERNAL') || die();


class prevaluation_import_csv_load_data extends gradeimport_csv_load_data {

    /**
     * OVERRIDE OF /grade/import/grade_import_form -> gradeimport_csv_load_data::prepare_import_grade_data()
     * Checks and prepares grade data for inserting into the gradebook.
     *
     * @param array $header Column headers of the CSV file.
     * @param object $formdata Mapping information from the preview page.
     * @param object $csvimport csv import reader object for iterating over the imported CSV file.
     * @param int $courseid The course ID.
     * @param bool $separatemode If we have groups are they separate?
     * @param mixed $currentgroup current group information.
     * @param bool $verbosescales Form setting for grading with scales.
     * @return bool True if the status for importing is okay, false if there are errors.
     */
    public function prepare_import_grade_data(
        $header,
        $formdata,
        $csvimport,
        $courseid,
        $separatemode,
        $currentgroup,
        $verbosescales
    ) {
        
        global $DB, $USER;
        
        // The import code is used for inserting data into the grade tables.
        $this->importcode = $formdata->importcode;
        $this->status = true;
        $this->headers = $header;
        $this->studentid = null;
        $this->gradebookerrors = null;
        $forceimport = $formdata->forceimport;
        // Temporary array to keep track of what new headers are processed.
        $this->newgradeitems = array();
        $this->trim_headers();
        $timeexportkey = null;
        $map = array();
        // Loops mapping_0, mapping_1 .. mapping_n and construct $map array.
        foreach ($header as $i => $head) {
            if (isset($formdata->{'mapping_'.$i})) {
                $map[$i] = $formdata->{'mapping_'.$i};
            }
            if ($head == get_string('timeexported', 'gradeexport_txt')) {
                $timeexportkey = $i;
            }
        }

        // If mapping information is supplied.
        $map[clean_param($formdata->mapfrom, PARAM_RAW)] = clean_param($formdata->mapto, PARAM_RAW);

        // Check for mapto collisions.
        $maperrors = array();
        foreach ($map as $i => $j) {
            if ($j == 0) {
                // You can have multiple ignores.
                continue;
            } else {
                if (!isset($maperrors[$j])) {
                    $maperrors[$j] = true;
                } else {
                    // Collision.
                    print_error('cannotmapfield', '', '', $j);
                }
            }
        }

        $this->raise_limits();

        $csvimport->init();
        $success = array();
        $failure = array();

        $index_email = $_POST['mapfrom'];
        $index_firstname = $_POST['mapfirstname'];
        $index_lastname = $_POST['maplastname'];
        $index_grade = $_POST['mapgrade'];
        
        while ($line = $csvimport->next())
        {
            foreach ($line as $key => $value) {

                $value = clean_param($value, PARAM_RAW);
                $value = trim($value);

                $csv_email = $line[$index_email];
                $csv_firstname = $line[$index_firstname];
                $csv_lastname = $line[$index_lastname];
                
                //$csv_grade = $line[$index_grade];
                $csv_grade = $this->csv_grade_cleanup($line[$index_grade]);

                /*
                 * the options are
                 * 1) userid, useridnumber, usermail, username - used to identify user row
                 * 2) new - new grade item
                 * 3) id - id of the old grade item to map onto
                 * 3) feedback_id - feedback for grade item id
                */

                // Explode the mapping for feedback into a label 'feedback' and the identifying number.
                $mappingbase = explode("_", $map[$key]);
                $mappingidentifier = $mappingbase[0];
                // Set the feedback identifier if it exists.
                if (isset($mappingbase[1])) {
                    $feedbackgradeid = (int)$mappingbase[1];
                } else {
                    $feedbackgradeid = '';
                }

                if($mappingidentifier !== "useremail") continue;

                $userid = $DB->get_record(
                    'user',
                    array(
                        'email' => $csv_email//$line['user_email']
                    )
                )->id;                

                $line['user_email'] = $csv_email;//$value;
                $line['course_id'] = $courseid;
                $line['user_id'] = $userid;

                if (!empty($value)) {
                    $this->studentid = $this->check_user_by_email($value, array(
                        'field' => 'email',
                        'label' => 'email address',
                    ));
                }
            }

            $userid = $DB->get_record(
                'user',
                array(
                    'email' => $csv_email//$line['user_email']
                )
            )->id;

            $line['user_id'] = $userid;
            $line['user_name'] = $csv_firstname;//$line[0];
            unset($line[$index_firstname]);//unset($line[0]);
            $line['user_surname'] = $csv_lastname;//$line[1];
            unset($line[$index_lastname]);//unset($line[1]);

            
            //$csv_grade = $this->csv_grade_cleanup($csv_grade);


            $line['user_grade'] = $csv_grade;//$line[3];
            unset($line[$index_grade]);//unset($line[3]);
            unset($line[$index_email]);//unset($line[2]); // email

            $context = get_context_instance(CONTEXT_COURSE, $line['course_id'], MUST_EXIST);
            $enrolled = is_enrolled($context, $userid, '', true);

            //handling absence of grade in CSV
            if($csv_grade == '')
            {
                // if not enrolled (or not in moodle), skip him
                if (is_null($userid) | is_null($enrolled) | !$enrolled)
                {
                    continue;
                }
                // if enrolled, remove actual grade
                else
                {
                    array_push($failure,$line);
                    prevaluation_remove_grade_activity(
                        array(
                            'user_id' => $userid,
                            'user_status' => 'in-moodle',
                            'user_email' => $csv_email,
                            'user_courseid' => $line['course_id']
                        ),
                        $DB,
                        $_POST['instanceid']
                    );
                }
            }
            else
            {
                if (is_null($userid) | is_null($enrolled) | !$enrolled)
                {
                    array_push($failure,$line);
                }
                else
                {
                    array_push($success,$line);
                }
            }
        }

        foreach ($success as $key => $line)
        {
            global $DB;
            $data['user_id'] = $line['user_id'];
            $data['user_status'] = 'in-moodle';
            $data['user_email'] = $line['user_email'];;
            $data['user_courseid'] = $line['course_id'];
            $data['user_grade'] = $line['user_grade'];
            if($data['user_grade'] !== '')
            {
                prevaluation_direct_grade_activity($data,$DB,$_POST['instanceid']);
            }
        }

        foreach($failure as $key => $user)
        {

            $table = 'prevaluation_waiting_users';
            $parameters = array(
                'course_id' => $user['course_id'],
                'user_email' => $user['user_email'],
                'instance_id' => $_POST['instanceid']
            );
            
            $record = new stdClass();
            $record->course_id          = $user['course_id'];
            $record->user_email         = $user['user_email'];
            $record->user_name          = $user['user_name'];
            $record->user_surname       = $user['user_surname'];
            $record->user_grade         = $user['user_grade'];
            $record->instance_id        = $_POST['instanceid'];

            $student = $DB->get_record(
                'user',
                array(
                    'email' => $record->user_email
                )
            );

            $record->user_id = $student->id;

            if ($DB->record_exists($table, $parameters))
            {
                $existing_record = $DB->get_record($table, $parameters);
                $existing_record_id = $existing_record->id;

                $DB->update_record(
                    $table,
                    array(
                        'id' => $existing_record_id,
                        'user_name' => $user['user_name'],
                        'user_surname' => $user['user_surname'],
                        'user_grade' => $user['user_grade']
                    ),
                    $bulk=false
                );
                //continue;//non aggiorno se già esiste
            }
            else
            {
                $lastinsertid = $DB->insert_record($table, $record, false);
            }
        }

        purge_all_caches();
        return $this->status;
    }



    /**
     * Cleans the GRADE field in CSV
     *
     * @param string $grade
     * @return string Returns grade cleaned according to plugin policy
     */
    protected function csv_grade_cleanup($csv_grade)
    {
        $csv_grade = preg_replace('/[^\d-]+/', '', $csv_grade); //remove all but numbers (signed or unsigned) in grades
        $csv_grade = trim(preg_replace('/\s+/', ' ', $csv_grade));

        return $csv_grade;
    }
  

    /**
     * Check that the user is in the system.
     *
     * @param string $value The value, from the csv file, being mapped to identify the user.
     * @param array $userfields Contains the field and label being mapped from.
     * @return int Returns the user ID if it exists, otherwise null.
     */
    protected function check_user_by_email($value,$userfields)
    {
        global $DB;
        $user = null;
        // The user may use the incorrect field to match the user. This could result in an exception.
        try {
            $user = $DB->get_record('user', array($userfields['field'] => $value));
        } catch (Exception $e) {
            return null;
        }
        // Field may be fine, but no records were returned.
        if (!$user || $usercheckproblem) {
            return null;
        }
        return $user->id;
    }

}